// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

USTRUCT()
struct FSpawnParameter
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
		float Time;
	UPROPERTY(EditAnywhere)
		float Distance;

	FSpawnParameter() : FSpawnParameter(0, 0) {}
	FSpawnParameter(float time, float distance) : Time(time), Distance(distance) {}
};

UCLASS()
class GEE_UE1_API AProjectile : public AActor
{
	GENERATED_BODY()
	
		/** Sphere collision component */
	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
		class USphereComponent* CollisionComp;

	/** Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
		class UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		float Damage;
public:	
	// Sets default values for this actor's properties
	AProjectile();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void LifeSpanExpired() override;

	UFUNCTION()
		void Shoot(const FVector& direction);

	UFUNCTION()
		void Destroy();

	/** called when projectile hits something */
	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);



	/** Returns CollisionComp subobject **/
	FORCEINLINE class USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ProjectileMovement subobject **/
	FORCEINLINE class UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }

	float GetDamage() const;

	UPROPERTY(EditAnywhere, Category = Projectile)
		FSpawnParameter DespawnParameter;

private:
	void reset();

	FVector m_spawnPosition;
	bool m_fired;
	bool m_hitFloor;
};
