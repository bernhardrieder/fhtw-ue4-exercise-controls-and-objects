// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Enemy.h"
#include <vector>
#include "EnemySpawner.generated.h"

UCLASS()
class GEE_UE1_API AEnemySpawner : public AActor
{
	GENERATED_BODY()

	class Respawn
	{
	public:
		AEnemy* Enemy;
		float DeltaTime;

		Respawn() : Respawn(nullptr, 0) {}
		Respawn(AEnemy* enemy) : Respawn(enemy, 0) {}
		Respawn(AEnemy* enemy, float deltaTime) : Enemy(enemy), DeltaTime(deltaTime) {}

		friend bool operator==(const Respawn & lhs, const Respawn & rhs)
		{
			return lhs.DeltaTime == rhs.DeltaTime && lhs.Enemy == rhs.Enemy;
		}
	};

	UPROPERTY(VisibleAnywhere, Category = Spawning)
		class UBoxComponent* WhereToSpawn;
public:	
	// Sets default values for this actor's properties
	AEnemySpawner();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//find a random point within the BoxComponent
	UFUNCTION()
		FVector GetRandomPointInVolume() const;

	void AddRespawnable(AEnemy* enemy);
	static AEnemySpawner* GetInstance() { return m_enemySpawnerInstance; }

protected:
	UPROPERTY(EditAnywhere, Category = Spawning)
		TSubclassOf<AEnemy> EnemyClass;

	UPROPERTY(EditAnywhere, Category = Spawning)
		int SpawnAtStart;

private:
	TArray<Respawn> m_respawnables;
	TArray<Respawn> m_respawnablesRemove;
	AActor* m_player;
	static AEnemySpawner* m_enemySpawnerInstance;

	void spawnEnemies();
	AEnemy* spawnEnemy(const FVector& spawnPos) const;
	void checkForRespawn(const float& DeltaTime);
	bool isPlayerDistanceBigEnough(AEnemy* enemy) const;
};
