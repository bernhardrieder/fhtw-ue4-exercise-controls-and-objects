// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "GEE_UE1.h"
#include "GEE_UE1GameMode.h"
#include "GEE_UE1HUD.h"
#include "GEE_UE1Character.h"

AGEE_UE1GameMode::AGEE_UE1GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AGEE_UE1HUD::StaticClass();
}
