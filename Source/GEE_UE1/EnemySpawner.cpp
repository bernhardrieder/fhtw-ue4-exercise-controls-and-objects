// Fill out your copyright notice in the Description page of Project Settings.

#include "GEE_UE1.h"
#include "EnemySpawner.h"
#include "Kismet/KismetMathLibrary.h"
#include "PoolManager.h"
#include "Enemy.h"
#include "GEE_UE1Character.h"

AEnemySpawner* AEnemySpawner::m_enemySpawnerInstance = nullptr;

// Sets default values
AEnemySpawner::AEnemySpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	WhereToSpawn = CreateDefaultSubobject<UBoxComponent>(TEXT("WhereToSpawn"));
	RootComponent = WhereToSpawn;

	SpawnAtStart = 30;
}

// Called when the game starts or when spawned
void AEnemySpawner::BeginPlay()
{
	// Find Actors by type (needs a UWorld object)
	for (TActorIterator<AGEE_UE1Character> It(GetWorld()); It; ++It)
	{
		m_player = *It;
		break;
	}
	Super::BeginPlay();
	m_enemySpawnerInstance = this;
	if (APoolManager::IsInitFinished())
		spawnEnemies();
	else
		APoolManager::OnFinishedPoolInit = [&]() { spawnEnemies(); };
}

// Called every frame
void AEnemySpawner::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	checkForRespawn(DeltaTime);
}

FVector AEnemySpawner::GetRandomPointInVolume() const
{
	FVector SpawnOrigin = WhereToSpawn->Bounds.Origin;
	FVector SpawnExtent = WhereToSpawn->Bounds.BoxExtent;

	return UKismetMathLibrary::RandomPointInBoundingBox(SpawnOrigin, SpawnExtent);
}

void AEnemySpawner::AddRespawnable(AEnemy* enemy)
{
	if (isPlayerDistanceBigEnough(enemy)) // case 2
		spawnEnemy(enemy->GetSpawnPosition());
	else
		m_respawnables.Push(Respawn(enemy));
}

void AEnemySpawner::spawnEnemies()
{
	for(int i = 0; i < SpawnAtStart; ++i)
	{
		AEnemy* enemy = spawnEnemy(GetRandomPointInVolume());
		if (!isPlayerDistanceBigEnough(enemy))
			APoolManager::Destroy(enemy);
	}
}

AEnemy* AEnemySpawner::spawnEnemy(const FVector& spawnPos) const
{
	AActor* actor = APoolManager::Instantiate(EnemyClass, spawnPos);
	AEnemy* enemy = Cast<AEnemy>(actor);
	enemy->SetSpawnPosition(spawnPos);
	return enemy;
}

void AEnemySpawner::checkForRespawn(const float& DeltaTime)
{
	if (m_respawnables.Num() == 0) return;

	for(int i = 0; i < m_respawnables.Num(); ++i)
	{
		m_respawnables[i].DeltaTime += DeltaTime;
		if(m_respawnables[i].DeltaTime >= m_respawnables[i].Enemy->RespawnParameter.Time &&
			isPlayerDistanceBigEnough(m_respawnables[i].Enemy)) // case 1 & 3
		{
			spawnEnemy(m_respawnables[i].Enemy->GetSpawnPosition());
			m_respawnablesRemove.Push(m_respawnables[i]);
		}
	}

	if (m_respawnablesRemove.Num() == 0) return;
	for(Respawn t : m_respawnablesRemove)
		m_respawnables.Remove(t);
	m_respawnablesRemove.Empty();

}

bool AEnemySpawner::isPlayerDistanceBigEnough(AEnemy* enemy) const
{
	float playerDistance = FVector::Dist(enemy->GetSpawnPosition(), m_player->GetActorLocation());
	return playerDistance >= enemy->RespawnParameter.Distance;
}
