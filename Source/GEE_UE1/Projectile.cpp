// Fill out your copyright notice in the Description page of Project Settings.

#include "GEE_UE1.h"
#include "Projectile.h"
#include "PoolManager.h"


// Sets default values
AProjectile::AProjectile()
{
	PrimaryActorTick.bCanEverTick = true;
	Damage = 10;
	DespawnParameter.Time = 0;
	DespawnParameter.Distance = 1000;
	m_spawnPosition = FVector::ZeroVector;
	m_fired = false; 
	m_hitFloor = false;

	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &AProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;
	ProjectileMovement->Bounciness = 0.3f;
	ProjectileMovement->Velocity = FVector::ZeroVector;
	ProjectileMovement->bUpdateOnlyIfRendered = true;

	InitialLifeSpan = 0.f; //means infinity
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	reset();
	Super::BeginPlay();
	//GLog->Log(FString("Projectile Lifespan = ") + FString::FromInt(GetLifeSpan()));
}

// Called every frame
void AProjectile::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	if(m_fired)
	{
		FVector distanceVector = GetActorLocation() - m_spawnPosition;
		//GLog->Log(FString("Distance Magnitude: ") + FString::FromInt(distanceVector.Size()));
		if (distanceVector.Size() >= DespawnParameter.Distance)
			Destroy();
	}
}

void AProjectile::LifeSpanExpired()
{
	//GLog->Log("Projectile Lifespan Expired!");
	Destroy();
}

void AProjectile::Shoot(const FVector& direction)
{
	if(ProjectileMovement)
	{
		ProjectileMovement->SetUpdatedComponent(CollisionComp);
		m_spawnPosition = GetActorLocation();
		SetLifeSpan(DespawnParameter.Time);
		m_fired = true;
		// set the projectile's velocity to the desired direction
		ProjectileMovement->Velocity = direction * ProjectileMovement->InitialSpeed;
		//GLog->Log("Shoot!");
	}
}

void AProjectile::Destroy()
{
	APoolManager::Destroy(this);
	reset();
}

void AProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Only add impulse and destroy projectile if we hit a physics
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && OtherComp->IsSimulatingPhysics())
	{
		OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());
	}
	else if(!m_hitFloor && OtherActor != nullptr && OtherComp != nullptr && OtherComp->ComponentHasTag("Floor"))
	{
		//GLog->Log("Projectile hits floor");
		m_hitFloor = true;
	}
}

float AProjectile::GetDamage() const
{
	return m_hitFloor ? 0 : Damage;
}

void AProjectile::reset()
{
	SetLifeSpan(0);
	m_spawnPosition = FVector::ZeroVector;
	m_fired = false;
	ProjectileMovement->Velocity = FVector::ZeroVector;
	m_hitFloor = false;
}
