// Fill out your copyright notice in the Description page of Project Settings.

#include "GEE_UE1.h"
#include "Enemy.h"
#include "PoolManager.h"
#include "EnemySpawner.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthPoints = 100.f;
	AttractionRadius = 1000.f;
	MaximumProximityDistance = 300.f;
	MovementSpeed = 200.f;
	ShootPauseInSeconds = 1.f;
	RespawnParameter.Time = 20.f;
	RespawnParameter.Distance = 400.f; 
	m_spawnPosition = FVector::ZeroVector;

	AttractionColliderComponent = CreateDefaultSubobject<USphereComponent>(TEXT("AttractionColliderComp"));
	AttractionColliderComponent->bGenerateOverlapEvents = true;
	AttractionColliderComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	AttractionColliderComponent->InitSphereRadius(5.0f);
	RootComponent = AttractionColliderComponent;

	Mesh = CreateDefaultSubobject<USceneComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
	MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	MuzzleLocation->SetupAttachment(Mesh);

	ColliderComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("ColliderComp"));
	ColliderComponent->InitBoxExtent(FVector(1, 1, 1));
	ColliderComponent->BodyInstance.SetCollisionProfileName("Enemy");
	ColliderComponent->OnComponentHit.AddDynamic(this, &AEnemy::OnHit);		// set up a notification for when this component hits something blocking
	ColliderComponent->SetupAttachment(Mesh);
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	m_maxHealth = HealthPoints;
	AttractionColliderComponent->SetSphereRadius(AttractionRadius);
	reset();
	Super::BeginPlay();
	
}

// Called every frame
void AEnemy::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	if(m_followPlayer && m_player)
	{
		followPlayer(DeltaTime);
		shootPlayer(DeltaTime);
	}

}

void AEnemy::NotifyActorBeginOverlap(AActor* Other)
{
	if(Other->ActorHasTag("Player"))
	{
		//GLog->Log("Player enters trigger");
		m_followPlayer = true;
		m_player = Other;
	}
}

void AEnemy::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor != nullptr && OtherComp != nullptr && OtherComp->GetCollisionProfileName().IsEqual("Projectile"))
	{
		AProjectile* projectile = Cast<AProjectile>(OtherActor);
		if (projectile)
		{
			HealthPoints -= projectile->GetDamage();
			projectile->Destroy();
			if (HealthPoints <= 0)
			{
				APoolManager::Destroy(this);
				AEnemySpawner::GetInstance()->AddRespawnable(this);
				reset();
			}
		}
	}
}

void AEnemy::SetSpawnPosition(const FVector& position)
{
	m_spawnPosition = position;
}

void AEnemy::reset()
{
	m_followPlayer = false;
	m_player = nullptr;
	HealthPoints = m_maxHealth;
	m_currentShotWait = ShootPauseInSeconds;
}

void AEnemy::followPlayer(float DeltaTime)
{
	FVector directonVectorToPlayer = m_player->GetActorLocation() - GetActorLocation();
	SetActorRotation(FRotationMatrix::MakeFromX(directonVectorToPlayer).Rotator());
	//GLog->Log(FString("Distance = ") + FString::FromInt(directonVectorToPlayer.Size()));
	if(directonVectorToPlayer.Size() >= MaximumProximityDistance)
	{
		FVector newPos = GetActorLocation() + GetActorForwardVector()*MovementSpeed*DeltaTime;
		SetActorLocation(FVector(newPos.X, newPos.Y, GetActorLocation().Z));
	}
}

void AEnemy::shootPlayer(float DeltaTime)
{
	m_currentShotWait += DeltaTime;
	if(m_currentShotWait >= ShootPauseInSeconds)
	{
		AActor* actor = APoolManager::Instantiate(ProjectileClass, MuzzleLocation->GetComponentLocation(), MuzzleLocation->GetComponentRotation());
		Cast<AProjectile>(actor)->Shoot(MuzzleLocation->GetForwardVector());
		m_currentShotWait = 0;
	}
}
