// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <map>
#include "GameFramework/Actor.h"
#include <functional>
#include "PoolManager.generated.h"

USTRUCT(BlueprintType)
struct FPoolingElement
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
		TSubclassOf<AActor> ActorBlueprint;

	UPROPERTY(EditAnywhere)
		int32 PoolSize;

	UPROPERTY(EditAnywhere)
		bool ReuseOldestObjectIfPoolDepleted;

	FPoolingElement()
	{
		ActorBlueprint = nullptr;
		PoolSize = 0;
		ReuseOldestObjectIfPoolDepleted = false;
	}
};

UCLASS()
class GEE_UE1_API APoolManager : public AActor
{
	GENERATED_BODY()
	
	struct Pool
	{
		Pool() : Pool(0, false) {}
		Pool(int poolSize, bool reuse) : Poolsize(poolSize), ReuseOldestObjectIfPoolDepleted(reuse) {}
		
		TArray<AActor*> Objects;
		TArray<AActor*> ActiveObjects;
		int Poolsize;
		bool ReuseOldestObjectIfPoolDepleted;
	};
public:	
	// Sets default values for this actor's properties
	APoolManager();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndplayReason) override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pool Manager")
		TArray<FPoolingElement> PoolingElements;

public:
	static AActor* Instantiate(const TSubclassOf<AActor>& original);
	static AActor* Instantiate(const TSubclassOf<AActor>& original, const FVector& position);
	static AActor* Instantiate(const TSubclassOf<AActor>& original, const FVector& position, const FRotator& rotation);
	static void Destroy(AActor* actor);
	static bool IsInitFinished();

	static std::function<void(void)> OnFinishedPoolInit;
	
private:
	static TMap<FString, Pool> m_pools;
	static class UWorld* m_world;
	static bool m_isInitFinished;

private:
	void initPools() const;
	static void addToPools(const TArray<FPoolingElement>& elements);
	static void setActorEnabled(AActor* someActor, bool enabled);
	static AActor* createActor(const TSubclassOf<AActor>& original, const FString& name);
};
