// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "GEE_UE1GameMode.generated.h"

UCLASS(minimalapi)
class AGEE_UE1GameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AGEE_UE1GameMode();
};



