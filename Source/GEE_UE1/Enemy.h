// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Projectile.h"
#include "Enemy.generated.h"

UCLASS()
class GEE_UE1_API AEnemy : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleDefaultsOnly, Category = Enemy)
		class USphereComponent* AttractionColliderComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = Enemy)
		class UBoxComponent* ColliderComponent;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Enemy)
		class USceneComponent* MuzzleLocation;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USceneComponent* Mesh;

public:	
	// Sets default values for this actor's properties
	AEnemy();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	virtual void NotifyActorBeginOverlap(AActor* Other) override;

	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	void SetSpawnPosition(const FVector& position);

	FVector GetSpawnPosition() { return m_spawnPosition; }

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		TSubclassOf<class AProjectile> ProjectileClass;

	UPROPERTY(EditAnywhere, Category = Enemy, meta = (ClampMin = "0.0", ClampMax = "500.0", UIMin = "0.0", UIMax = "500.0"))
		float HealthPoints;

	UPROPERTY(EditAnywhere, Category = Enemy)
		float AttractionRadius;

	UPROPERTY(EditAnywhere, Category = Enemy)
		float MaximumProximityDistance;

	UPROPERTY(EditAnywhere, Category = Enemy)
		float MovementSpeed;

	UPROPERTY(EditAnywhere, Category = Projectile)
		float ShootPauseInSeconds;

	UPROPERTY(EditAnywhere, Category = Enemy)
		FSpawnParameter RespawnParameter;

private:
	float m_maxHealth;
	float m_currentShotWait;
	FVector m_spawnPosition;
	bool m_followPlayer;
	AActor* m_player;

	void reset();
	void followPlayer(float DeltaTime);
	void shootPlayer(float DeltaTime);
};
