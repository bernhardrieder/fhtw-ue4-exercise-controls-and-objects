// Fill out your copyright notice in the Description page of Project Settings.

#include "GEE_UE1.h"
#include "PoolManager.h"
#include "Projectile.h"


UWorld* APoolManager::m_world = nullptr;
TMap<FString, APoolManager::Pool> APoolManager::m_pools;
std::function<void(void)> APoolManager::OnFinishedPoolInit = nullptr;
bool APoolManager::m_isInitFinished = false;

// Sets default values
APoolManager::APoolManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void APoolManager::BeginPlay()
{
	m_world = GetWorld();
	if(!m_world)
		GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, TEXT("World not found in Pool Manager!"));

	initPools();

	Super::BeginPlay();
}

void APoolManager::EndPlay(const EEndPlayReason::Type EndplayReason)
{
	for (auto e : PoolingElements)
	{
		FString name = e.ActorBlueprint->GetName();

		for (AActor* actor : m_pools[name].Objects)
		{
			m_world->DestroyActor(actor);
		}
		for (AActor* actor : m_pools[name].ActiveObjects)
		{
			m_world->DestroyActor(actor);
		}
	}
}

AActor* APoolManager::Instantiate(const TSubclassOf<AActor>& original)
{
	return APoolManager::Instantiate(original, original->GetDefaultObject<AActor>()->GetActorTransform().GetLocation());
}

AActor* APoolManager::Instantiate(const TSubclassOf<AActor>& original, const FVector& position)
{
	return APoolManager::Instantiate(original, position, original->GetDefaultObject<AActor>()->GetActorTransform().GetRotation().Rotator());
}

AActor* APoolManager::Instantiate(const TSubclassOf<AActor>& original, const FVector& position, const FRotator& rotation)
{
	AActor* actor;
	FString name = original->GetName();

	if(m_pools.Contains(name) && m_pools[name].Objects.Num() >= 1)
	{
		//GLog->Log(FString("Instantiate: ") + name);
		actor = m_pools[name].Objects.Pop();
		m_pools[name].ActiveObjects.Push(actor);
		actor->SetActorLocationAndRotation(position, rotation);
		setActorEnabled(actor, true);
		return actor;
	}

	if(m_pools.Contains(name) && !m_pools[name].ReuseOldestObjectIfPoolDepleted && m_pools[name].Objects.Num() == 0)
	{
		GLog->Log(FString("pool size too small: ") + name);
		actor = createActor(original, name);
		m_pools[name].ActiveObjects.Push(actor);
		actor->SetActorLocationAndRotation(position, rotation);
		setActorEnabled(actor, true);
		return actor;
	}

	if (m_pools.Contains(name) && m_pools[name].ReuseOldestObjectIfPoolDepleted && m_pools[name].Objects.Num() == 0)
	{
		GLog->Log(FString("reuse oldest object: ") + name);
		if(m_pools[name].ActiveObjects.Num() > 1 && m_pools[name].ActiveObjects[0])
		{
			APoolManager::Destroy(m_pools[name].ActiveObjects[0]);
			return Instantiate(original, position, rotation);
		}
	}

	GLog->Log(FString("object not in pool: ") + name );
	m_pools.Add(name, Pool(2, false));
	actor = createActor(original, name);
	APoolManager::Destroy(actor);
	return Instantiate(original, position, rotation);
}

void APoolManager::Destroy(AActor* actor)
{
	setActorEnabled(actor, false);
	if(m_pools.Contains(actor->GetActorLabel()))
	{
		m_pools[actor->GetActorLabel()].Objects.Push(actor);
		m_pools[actor->GetActorLabel()].ActiveObjects.Remove(actor);
	}
	else
	{
		m_world->DestroyActor(actor);
	}
	actor = nullptr;
}

bool APoolManager::IsInitFinished()
{
	return m_isInitFinished;
}

void APoolManager::initPools() const
{
	addToPools(PoolingElements);
	m_isInitFinished = true;
	if (OnFinishedPoolInit != nullptr)
		OnFinishedPoolInit();
}

void APoolManager::addToPools(const TArray<FPoolingElement>& elements)
{
	AActor* actor;
	FString name;

	for(auto e : elements)
	{
		if (e.ActorBlueprint == nullptr)
		{
			GEngine->AddOnScreenDebugMessage(-1, 20.0f, FColor::Red, TEXT("Actor Blueprint nullptr!!!"));
			continue;
		}
		name = e.ActorBlueprint->GetName();

		if (!m_pools.Contains(name))
		{
			m_pools.Add(name, Pool(e.PoolSize, e.ReuseOldestObjectIfPoolDepleted));
		}

		for (int i = 0; i < e.PoolSize; ++i)
		{
			actor = createActor(e.ActorBlueprint, name);
			m_pools[name].Objects.Push(actor);
			setActorEnabled(actor, false);
		}
	}
}

void APoolManager::setActorEnabled(AActor* someActor, bool enabled)
{
	someActor->SetActorTickEnabled(enabled);
	someActor->SetActorEnableCollision(enabled);
	someActor->SetActorHiddenInGame(!enabled);
}

AActor* APoolManager::createActor(const TSubclassOf<AActor>& original, const FString& name)
{
	AActor* actor = m_world->SpawnActor<AActor>(original);
	//GLog->Log(FString("create actor with name: ") + name);
#if WITH_EDITOR
	actor->SetFolderPath("/PoolManager");
#endif
	actor->SetActorLabel(name);

	return actor;
}
